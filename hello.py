#!/usr/bin/env python3

import cgi
import html
import sys
import codecs
import psycopg2
import datetime
from psycopg2 import sql

#Кодировка utf-8

sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())

'''Обработка формы'''

form = cgi.FieldStorage()
text1 = form.getvalue("field-name-1", "")
text1 = html.escape(text1)
text2 = form.getvalue("field-email", "")

date1 = datetime.datetime.strptime(form.getvalue("field-date"), "%Y-%m-%d")
date1 = date1.date()

if form.getvalue('check-1'):
   checkbox_flag = True
else:
   checkbox_flag = False
   print("Content-type: text/html")
   print("Set-cookie: name=value")
   print()
   print('''<DOCTYPE html>
   <html lang="ru">
   <head>
   <title>backend-3</title>
   <meta charset="UTF-8">
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
   </head>
   <body>
   <h1>Форма заполнена с ошибкой и не отправлена!</h1>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
   </body>
   </html>
   ''')
   exit()

sex = form.getvalue('radio-group-1')

count = form.getvalue('radio-group-2')
count = int(count)

if form.getvalue('field-name-2'):
    bio = form.getvalue('field-name-2')
else:
    bio = ''

if form.getvalue('field-name-4'):
   subject = form.getvalue('field-name-4')
else:
   subject = "Not entered"

if subject == "Not entered" or bio == '' or text1 == '' or text2 == '':
    print('''<DOCTYPE html>
       <html lang="ru">
       <head>
       <title>backend-3</title>
       <meta charset="UTF-8">
       <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
       </head>
       <body>
       <h1>Форма заполнена с ошибкой и не отправлена!</h1>
       <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
       </body>
       </html>
       ''')
    exit()

'''Тут работа с БД'''
conn = psycopg2.connect(
    dbname='postgres',
    user='myuser',
    password='UigfjL0f',
    host='localhost'
)
with conn.cursor() as cursor:
    print("Content-type: text/html")
    print("Set-cookie: name=value")
    print('Выполняется')
    conn.autocommit = True
    values = [
        (1, text1, text2, date1, sex, count, subject, bio, checkbox_flag),
    ]
    insert = sql.SQL('INSERT INTO form (id_person, first_name, email, nowdate, sex, kolvo, abilities, bio, checkbox) VALUES {}').format(
        sql.SQL(',').join(map(sql.Literal, values))
    )
    cursor.execute(insert)
conn.close()


'''HTML разметка'''

print("Content-type: text/html")
print("Set-cookie: name=value")
print()
print('''<DOCTYPE html>
<html lang="ru">
<head>
<title>backend-3</title>
<meta charset="UTF-8">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>
<body>
<h1>Форма успешно отправлена!</h1>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>
''')